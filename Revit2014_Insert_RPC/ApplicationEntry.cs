﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;

namespace AU2013
{
    namespace Revit2014
    {
        public class ApplicationEntry : IExternalApplication
        {

            #region Members

            private Dictionary<string, string> m_familyList = new Dictionary<string, string>();
            private Document m_document;

            #endregion

            #region IExternalApplication Implementations

            public Result OnShutdown(UIControlledApplication application)
            {
                application.ControlledApplication.DocumentOpened -= new EventHandler
                   <Autodesk.Revit.DB.Events.DocumentOpenedEventArgs>(ControlledApplication_DocumentOpened);

                application.ControlledApplication.DocumentCreated -= new EventHandler
                <Autodesk.Revit.DB.Events.DocumentCreatedEventArgs>(ControlledApplication_DocumentCreated);

                return Result.Succeeded;
            }

            public Result OnStartup(UIControlledApplication application)
            {

                try
                {
                    application.ControlledApplication.DocumentOpened += new EventHandler
                        <Autodesk.Revit.DB.Events.DocumentOpenedEventArgs>(ControlledApplication_DocumentOpened);

                    application.ControlledApplication.DocumentCreated += new EventHandler
                        <Autodesk.Revit.DB.Events.DocumentCreatedEventArgs>(ControlledApplication_DocumentCreated);
                }

                catch (Exception)
                {
                    return Result.Failed;
                }

                AddCustomPanel(application);

                return Result.Succeeded;
            }

            #endregion

            #region Methods

            private void AddCustomPanel(UIControlledApplication application)
            {
                // create a panel named "Events Monitor";
                string panelName = "AU2013 ArchVision Plug-in";
                // create a button on the panel.
                RibbonPanel ribbonPanelPushButtons = application.CreateRibbonPanel(panelName);
                PushButtonData pushButtonData = new PushButtonData("EventsSetting",
                    "Insert RPC", System.Reflection.Assembly.GetExecutingAssembly().Location,
                    "AU2013.Revit2014.InsertCommand");
                PushButton pushButtonCreateRPC = ribbonPanelPushButtons.AddItem(pushButtonData) as PushButton;
                pushButtonCreateRPC.ToolTip = "Setting Events";
            }

            protected void ControlledApplication_DocumentCreated(object sender, Autodesk.Revit.DB.Events.DocumentCreatedEventArgs e)
            {
                m_document = e.Document;
                if (m_document.IsFamilyDocument)
                    return;
            }

            protected void ControlledApplication_DocumentOpened(object sender, Autodesk.Revit.DB.Events.DocumentOpenedEventArgs e)
            {
                m_document = e.Document;
                if (m_document.IsFamilyDocument)
                    return;
            }
            #endregion

        }
    }
}
