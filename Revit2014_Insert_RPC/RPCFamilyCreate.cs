﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Autodesk.Revit.DB;

//namespace AU2013
//{
//    class RPCFamilyCreate
//    {
//        private Autodesk.Revit.UI.UIDocument m_uiDoc;
//        private Autodesk.Revit.DB.Document m_revitDoc;
//        private Autodesk.Revit.DB.Document m_FamilyDoc;
//        private Autodesk.Revit.ApplicationServices.Application m_application;


//        /// <summary>
//        /// A default Render Appearance parameter: Buddha Belly Bamboo.
//        /// </summary>
//        private string _newRenderAppearance = "";

//        /// <summary>
//        /// The Render Appearance Properties to be set by Revit.
//        /// </summary>
//        private String _newRenderAppearanceProperties = "";

//        private string m_RPCFamilyTemplateFile = "";

//        private string m_newFamilyName = "RPC Drag and Drop";

//        /// <summary>
//        /// The default location for the inserted RPC content.
//        /// </summary>
//        private XYZ _location = XYZ.Zero;

//        public RPCFamilyCreate(Autodesk.Revit.UI.UIDocument uiDoc)
//        {
//            m_uiDoc = uiDoc;
//            m_revitDoc = uiDoc.Document;
//            m_application = m_revitDoc.Application;

//            //Load the path to the RPC Family.rft
//            string familyTemplateDir = m_revitDoc.Application.FamilyTemplatePath;
//            foreach (string familyFile in System.IO.Directory.EnumerateFileSystemEntries(familyTemplateDir, "*RPC*.rft", System.IO.SearchOption.AllDirectories))
//            {
//                m_RPCFamilyTemplateFile = familyFile;
//            }
//        }

//        public void CreateRPCFamily(string name, string guid)
//        {
//            m_newFamilyName = name;
//            string renderAppearence = guid;
//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:NewFamilyDocument - START");
//            m_FamilyDoc = m_application.NewFamilyDocument(m_RPCFamilyTemplateFile);
//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:new Transaction - START");
//            Transaction familyEditorTransaction = new Transaction(m_FamilyDoc, "ChangeRPCRenderAppearance");
//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily: familyEditorTransaction.Start() - START");
//            familyEditorTransaction.Start();

//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:  Family family = m_FamilyDoc.OwnerFamily; - START");
//            Family family = m_FamilyDoc.OwnerFamily;

//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:   FamilyManager familyManager = m_FamilyDoc.FamilyManager; - START");
//            FamilyManager familyManager = m_FamilyDoc.FamilyManager;
//            //this is not strictly necessary, but insures the RPC family is imported with an identifiable name
//            FamilyType newFamilyType = familyManager.NewType(m_newFamilyName);

//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:  MendTemplateGeometry; - START");
//            MendTemplateGeometry(); //this fixes the issue where the element geometry is not imported into revit

//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:   FamilyParameter renderAppearancePropertiesParam = familyManager.get_Parameter(\"Render Appearance Properties\");; - START");
//            FamilyParameter renderAppearancePropertiesParam = familyManager.get_Parameter("Render Appearance Properties");

//            if (null != renderAppearancePropertiesParam)
//            {
//                familyManager.Set(renderAppearancePropertiesParam, _newRenderAppearanceProperties);
//            }

//            FamilyParameter renderAppearanceParam = familyManager.get_Parameter("Render Appearance");
//            if (null != renderAppearanceParam)
//            {
//                familyManager.Set(renderAppearanceParam, renderAppearence);
//            }

//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily: familyEditorTransaction.Commit(); - START");
//            familyEditorTransaction.Commit();
//            familyEditorTransaction.Dispose();

//        }

//        public void InsertRPCFamilyInstance()
//        {
//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily: !m_FamilyDoc.IsFamilyDocument - START");
//            if (!m_FamilyDoc.IsFamilyDocument)
//            {
//                System.Diagnostics.Debug.WriteLine("CreateRPCFamily: RETURN - START");
//                return;
//            }


//            Transaction projectTransaction = new Transaction(m_revitDoc);
//            projectTransaction.Start("InsertFamilyIntoProject");
        
//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:  Family newFamily = m_FamilyDoc.LoadFamily(m_revitDoc); - START");
//            Family newFamily = m_FamilyDoc.LoadFamily(m_revitDoc);
//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:  FamilySymbolSetIterator fss = newFamily.Symbols.ForwardIterator(); - START");
//            FamilySymbolSetIterator fss = newFamily.Symbols.ForwardIterator();

//            while (fss.MoveNext())
//            {
//                FamilySymbol currentFamilySymbol = fss.Current as FamilySymbol;
//                System.Diagnostics.Debug.WriteLine("CreateRPCFamily: m_revitDoc.Create.NewFamilyInstance(_location, currentFamilySymbol, Autodesk.Revit.DB.Structure.StructuralType.NonStructural);");
//                //m_revitDoc.Create.NewFamilyInstance(_location, currentFamilySymbol, Autodesk.Revit.DB.Structure.StructuralType.NonStructural);

//                m_uiDoc.PromptForFamilyInstancePlacement(currentFamilySymbol);

//                //TaskDialog.Show("Family Instance Inserted", "Successfully Inserted Family Instance: " + currentFamilySymbol.Name);
//            }

//            System.Diagnostics.Debug.WriteLine("CreateRPCFamily:m_FamilyDoc.Close(false); //closes the open family document");
//            m_FamilyDoc.Close(false); //closes the open family document

//            projectTransaction.Commit();
//            projectTransaction.Dispose();
//        }

//        /// <summary>This method is used to fix a bug in the family editor
//        /// which causes the geometry for rpc content not to be imported 
//        /// into a revit project.  Simply toggles one parameter for the family.
//        /// </summary>
//        private void MendTemplateGeometry()
//        {
//            Family family = m_FamilyDoc.OwnerFamily;

//            //"Render Appearance Source" Integer Values:
//            //default setting:   4
//            int familyGeometryValue = 3;
//            int thirdPartyValue = 4;

//            Parameter familyParam = family.get_Parameter("Render Appearance Source");
//            familyParam.Set(familyGeometryValue);
//            familyParam.Set(thirdPartyValue);
//        }


//    }
//}

namespace AU2013
{
    class RPCFamilyCreate
    {
        #region Members
        private Autodesk.Revit.UI.UIDocument m_uiDocument;
        private Autodesk.Revit.DB.Document m_revitDocument;
        private Autodesk.Revit.DB.Document m_familyDocument;
        private Autodesk.Revit.ApplicationServices.Application m_application;
        private string renderAppearanceProperties;
        private string familyTemplate;
        private string familyName;
        private string renderAppearance;
        private XYZ location;
        private string m_folderPath;
        private bool m_bOpenedExistingRFA = false;
        private bool m_bFirstLoad = true;

        //private ILog log = LogManager.GetLogger(typeof(RPCFamilyCreate));
        #endregion

         public RPCFamilyCreate(Autodesk.Revit.UI.UIDocument uiDoc)
        {
            m_uiDocument = uiDoc;
            m_revitDocument = uiDoc.Document;
            m_application = m_revitDocument.Application;
            ////log.Debug("RPCFamilyCreate - Constructor");
            //Initialize member variables
            //revitDocument = revitDoc;
            //application = revitDocument.Application;
            renderAppearanceProperties = String.Empty;
            renderAppearanceProperties = String.Empty;
            m_folderPath = string.Empty;
            location = XYZ.Zero;
            familyName = "RPC Drag and Drop";

            InitializeFolder();
            //Get path to family template directory
            string familyTemplateDir = m_revitDocument.Application.FamilyTemplatePath;
            ////log.Debug("familyTemplateDir -" + familyTemplateDir);
            //Find the RPC family template file
            foreach (string familyFile in System.IO.Directory.EnumerateFileSystemEntries(familyTemplateDir, "*RPC*.rft", System.IO.SearchOption.AllDirectories))
                familyTemplate = familyFile;

            ////log.Debug("familyTemplate - " + familyTemplate);
        }

        public void CreateRPCFamily(string name, string guid)
        {
            //Set
            foreach (string c in new List<string>() { "[", "]", "{", "}", "-", "+", "_", "=", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "/", "\\", "'", "`" })
            {
                if (c == " & " || c == "&")
                    name = name.Replace(c, " and ");
                else
                    name = name.Replace(c, "");
            }
            familyName = name;
            renderAppearance = guid;

            ////log.Debug(string.Format("CreateRPCFamily - name:{0} , guid:{1}", name, guid));

            //Create new family document
            if (!GetDocument())
            {
               // //log.Debug("CreateRPCFamily - Failed to open family document.");
                return;
            }

            if (m_familyDocument == null)
            {
               // log.Error("CreateRPCFamily - Failed to open a family document.");
                throw new Exception("Cannot open RPC Family Document");
            }

            //Transaction surrounding the new family document (so it can be edited).
            Transaction familyEditorTransaction = new Transaction(m_familyDocument);
            familyEditorTransaction.Start("ChangeRPCRenderAppearance");

            //Get family information
            Family family = m_familyDocument.OwnerFamily;
            FamilyManager familyManager = m_familyDocument.FamilyManager;
            FamilyType newFamilyType = familyManager.NewType(familyName);  //AUTODESK INTERN SAYS: this is not strictly necessary, but insures the RPC family is imported with an identifiable name
            MendTemplateGeometry(); //AUTODESK INTERN SAYS: this fixes the issue where the element geometry is not imported into revit
            //FamilyParameter renderAppearancePropertiesParam = familyManager.get_Parameter("Render Appearance Properties");
            //FamilyParameter renderAppearanceParam = familyManager.get_Parameter("Render Appearance");


            //if (null != renderAppearancePropertiesParam)
            ////log.Debug("CreateRPCFamily - renderAppearanceProperties" + renderAppearanceProperties);
            FamilyHelper.RawSetFamilyParameterValue(familyManager, familyName, "Render Appearance Properties", renderAppearanceProperties);
            //familyManager.Set(renderAppearancePropertiesParam, renderAppearanceProperties);

            //if (null != renderAppearanceParam)
            ////log.Debug("CreateRPCFamily - renderAppearance" + renderAppearance);
            FamilyHelper.RawSetFamilyParameterValue(familyManager, familyName, "Render Appearance", renderAppearance);
            //familyManager.Set(renderAppearanceParam, renderAppearence);

            //Finished editing the Family Document (Commit changes)
            familyEditorTransaction.Commit();
            //familyEditorTransaction.Dispose();

            try
            {
                CloseDocument();
            }
            catch (Exception e)
            {
                //log.Error("Error closing document.  " + e.ToString());
            }

            //saveAsFileName = @"C:\Temp\" + familyName + ".rfa";
            //for (int ct = 0; System.IO.File.Exists(saveAsFileName); ct++)
            //{
            //    //Keep renaming it
            //    saveAsFileName = @"C:\Temp\" + familyName + ct + ".rfa";
            //}

            //familyDocument.SaveAs(saveAsFileName);
            //familyDocument.Close();
        }

        bool GetDocument()
        {
            ////log.Debug(string.Format("GetDocument - Does family exist?? - {0}", m_folderPath + familyName + ".rfa"));
            if (System.IO.File.Exists(m_folderPath + familyName + ".rfa"))
            {
              //  //log.Debug(string.Format("GetDocument - Does family exist?? Yes - {0}", m_folderPath + familyName + ".rfa"));
                return false;
            }
            else
            {
                ////log.Debug(string.Format("GetDocument - Does family exist?? No - {0}", m_folderPath + familyName + ".rfa"));
                ////log.Debug("Creating new family document. Template : " + familyTemplate);
                m_familyDocument = m_application.NewFamilyDocument(familyTemplate);
                return true;
            }
        }

        void CloseDocument()
        {
            if (m_bOpenedExistingRFA)
                m_familyDocument.Save();
            else
                m_familyDocument.SaveAs(m_folderPath + familyName + ".rfa");

            m_familyDocument.Close();
        }

        Family LoadFamily()
        {
            ////log.Debug("LoadFamily - Start");
            Family ret = null;

            try
            {
                FilteredElementCollector collector = new FilteredElementCollector(m_revitDocument);

                collector.OfCategory(BuiltInCategory.OST_Entourage);

                collector.OfClass(typeof(FamilySymbol));

                FilteredElementIterator it = collector.GetElementIterator();

                int i = 0;
                while (it.MoveNext())
                {
              //      //log.Debug("LoadFamily it.MoveNext() - " + ++i);
                    FamilySymbol fs = it.Current as FamilySymbol;
                    if (fs.Name == familyName)
                    {
                        ret = fs.Family;
                //        //log.Debug(string.Format("LoadFamily break - {0} - {1}", fs.Name, familyName));
                        break;
                    }
                }

                if (ret != null)
                {
                  //  //log.Debug("LoadFamily - Mid : " + ret.Name);
                    return ret;
                }

                //Transaction surrounding the Revit document (so that the family can be placed into it).
                Transaction projectTransaction = new Transaction(m_revitDocument);
                projectTransaction.Start("InsertFamilyIntoProject");


                //if (m_bFirstLoad)
                // {
                ////log.Debug("LoadFamily - firstLoad == true");
                ////log.Debug("LoadFamily - loading family - " + m_folderPath + familyName + ".rfa");
                m_revitDocument.LoadFamily(m_folderPath + familyName + ".rfa", out ret);
                // }
                // else
                // {
                //     //log.Debug("LoadFamily - loading family - " + m_folderPath + familyName + ".rfa");
                //     revitDocument.LoadFamily(m_folderPath + familyName + ".rfa", new famLoadOptions(), out ret);
                // }

                projectTransaction.Commit();
            }
            catch (Exception ex)
            {
                //log.Error("Load family failed - " + ex.ToString());
            }
           // //log.Debug("LoadFamily - End : " + ret.Name);
            return ret;
        }

        public FamilyInstance InsertRPCFamilyInstance(Autodesk.Revit.UI.UIDocument uidoc)
        {
            //Validate the family documents
            //if (!familyDocument.IsFamilyDocument)
            //{
            //    System.Diagnostics.Debug.WriteLine("Error: Non-Family document.");
            //    return;
            //}
           // //log.Debug("InsertRPCFamilyInstance");
            FamilyInstance ret = null;

            Family newFamily = LoadFamily();

            try //This is the block which breaks on Beta2 and Release!!!
            {
                //Load family object into the Revit document (the scene).

                FamilySymbolSetIterator fss = newFamily.Symbols.ForwardIterator();
                while (fss.MoveNext())
                {
                    FamilySymbol currentFamilySymbol = fss.Current as FamilySymbol;
                    uidoc.PromptForFamilyInstancePlacement(currentFamilySymbol);
                    //log.Debug("InsertRPCFamilyInstance - break from placement.");
                    break;
                }
            }
            catch (Exception ex)
            {
                //log.Error(ex.ToString());
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return ret;
            }
            finally
            {
                //closes the open family document
                //familyDocument.Close(false); 
                //projectTransaction.Dispose();
                //Finished editing the Project Document (Commit changes)
                //projectTransaction.Commit();
                //projectTransaction.Dispose();

            }
            return ret;
        }

        /// <summary>This method is used to fix a bug in the family editor
        /// which causes the geometry for rpc content not to be imported 
        /// into a revit project.  Simply toggles one parameter for the family.
        /// </summary>
        private void MendTemplateGeometry()
        {
            Family family = m_familyDocument.OwnerFamily;

            //"Render Appearance Source" Integer Values:
            //default setting:   4
            int familyGeometryValue = 3;
            int thirdPartyValue = 4;

            Parameter familyParam = family.get_Parameter("Render Appearance Source");
            familyParam.Set(familyGeometryValue);
            familyParam.Set(thirdPartyValue);
        }

        private void InitializeFolder()
        {
            //Get path to family template directory
            string familyTemplateDir = m_revitDocument.Application.FamilyTemplatePath;
            //Find the RPC family template file
            foreach (string familyFile in System.IO.Directory.EnumerateFileSystemEntries(familyTemplateDir, "*RPC*.rft", System.IO.SearchOption.AllDirectories))
                familyTemplate = familyFile;

            m_folderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\ArchVision\\AVRevitPlugin\\";

            if (!System.IO.Directory.Exists(m_folderPath))
                System.IO.Directory.CreateDirectory(m_folderPath);
        }
    }

    //PER AUTODESK:
    class famLoadOptions : IFamilyLoadOptions
    {
        bool IFamilyLoadOptions.OnFamilyFound(bool familyInUse, out bool overwriteParameterValues)
        {
            overwriteParameterValues = true;
            return true;
        }

        bool IFamilyLoadOptions.OnSharedFamilyFound(Family sharedFamily, bool familyInUse, out FamilySource source, out bool overwriteParameterValues)
        {
            source = FamilySource.Family;
            overwriteParameterValues = false;
            return false;
        }
    }
}
