﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Autodesk.Revit;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.Creation;
using Autodesk.Revit.Attributes;

namespace AU2013.Revit2014
{
    [TransactionAttribute(TransactionMode.Manual)]
    class InsertCommand : IExternalCommand
    {

        public Result Execute(ExternalCommandData commandData,
            ref string message,
            ElementSet elements)
        {
            try
            {
                RPCFamilyCreate rpc = new RPCFamilyCreate(commandData.Application.ActiveUIDocument);
                rpc.CreateRPCFamily("Tina Chair", "RPC-8-EA23-WR69-HC80-C");
                rpc.InsertRPCFamilyInstance(commandData.Application.ActiveUIDocument);
            }
            catch (Autodesk.Revit.Exceptions.OperationCanceledException)
            {
                return Result.Cancelled;
            }

            catch (Exception ex)
            {
                message = ex.Message;
                return Result.Failed;
            }

            return Result.Succeeded;
        }
    }
}
