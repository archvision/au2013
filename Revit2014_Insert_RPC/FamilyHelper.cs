﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Autodesk.Revit.DB;

namespace AU2013
{
    class FamilyHelper
    {
        public static List<T> RawConvertSetToList<T>(IEnumerable set)
        {
            List<T> list = (from T p in set select p).ToList<T>();
            return list;
        }
        public static FamilyParameter RawFindFamilyParameter(FamilyManager fm, string parameterName)
        {
            FamilyParameter fp = RawConvertSetToList<FamilyParameter>(fm.Parameters).
                FirstOrDefault(e => e.Definition.Name.Equals(parameterName, StringComparison.CurrentCultureIgnoreCase));
            if (fp == null) throw new Exception("Invalid ParameterName Input!");

            return fp;
        }

        public static FamilyType RawFindFamilyType(FamilyManager fm, string familyTypeName)
        {
            FamilyType famType = RawConvertSetToList<FamilyType>(fm.Types).
                FirstOrDefault(e => e.Name.Equals(familyTypeName, StringComparison.CurrentCultureIgnoreCase));
            if (famType == null) throw new Exception("Invalid FamilyTypeName Input!");

            return famType;
        }

        public static void RawSetFamilyParameterValue(FamilyManager fm, string familyTypeName, string parameterName, object value)
        {
            RawSetFamilyParameterValue(fm, RawFindFamilyType(fm, familyTypeName), RawFindFamilyParameter(fm, parameterName), value);
        }

        public static void RawSetFamilyParameterValue(FamilyManager fm, FamilyType ft, string parameterName, object value)
        {
            RawSetFamilyParameterValue(fm, ft, RawFindFamilyParameter(fm, parameterName), value);
        }

        public static void RawSetFamilyParameterValue(FamilyManager fm, string familyTypeName, FamilyParameter fp, object value)
        {
            RawSetFamilyParameterValue(fm, RawFindFamilyType(fm, familyTypeName), fp, value);
        }

        public static void RawSetFamilyParameterValue(FamilyManager fm, FamilyType ft, FamilyParameter fp, object value)
        {
            FamilyType curFamType = fm.CurrentType;
            fm.CurrentType = ft;

            try
            {
                switch (fp.StorageType)
                {
                    case StorageType.None:
                        break;
                    case StorageType.Double:
                        if (value.GetType().Equals(typeof(string)))
                        {
                            fm.Set(fp, double.Parse(value as string));
                        }
                        else
                        {
                            fm.Set(fp, Convert.ToDouble(value));
                        }
                        break;
                    case StorageType.Integer:
                        if (value.GetType().Equals(typeof(string)))
                        {
                            fm.Set(fp, int.Parse(value as string));
                        }
                        else
                        {
                            fm.Set(fp, Convert.ToInt32(value));
                        }
                        break;
                    case StorageType.ElementId:
                        if (value.GetType().Equals(typeof(ElementId)))
                        {
                            fm.Set(fp, value as ElementId);
                        }
                        else if (value.GetType().Equals(typeof(string)))
                        {
                            fm.Set(fp, new ElementId(int.Parse(value as string)));
                        }
                        else
                        {
                            fm.Set(fp, new ElementId(Convert.ToInt32(value)));
                        }
                        break;
                    case StorageType.String:
                        fm.Set(fp, value.ToString());
                        break;
                }
            }
            catch
            {
                throw new Exception("Invalid Value Input!");
            }
            finally
            {
                fm.CurrentType = curFamType;
            }
        }
    }
}
